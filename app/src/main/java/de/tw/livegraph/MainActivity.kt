package de.tw.livegraph

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.viewinterop.AndroidView
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import de.tw.livegraph.ui.theme.LiveGraphTheme
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.*
import dagger.hilt.android.AndroidEntryPoint
import de.tw.magnetometerapp.data.SensorModel


@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LiveGraphTheme {
                val mainViewModel = viewModel<MainViewModel>()
                val sensorData = mainViewModel.sensorDownFlow.collectAsState(initial = SensorModel(0F,0F))
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Column() {
                        Text("X: ${sensorData.value.x} --- Y: ${sensorData.value.y}")
                        MagLineChart()
                    }
                }
            }
        }
    }
}

@Composable
fun MagLineChart() {
    val mainViewModel = viewModel<MainViewModel>()
    val sensorData = mainViewModel.sensorDownFlow.collectAsState(SensorModel(0F,0F)).value
    //val index: MutableState<Float> = remember { mutableStateOf(0f) }

    AndroidView(modifier = Modifier.fillMaxSize(),
        factory = { context ->
            val lineChart = LineChart(context)
            var entries = listOf(Entry(1f,1f)) //emptyList<Entry>()//

            val dataSet = LineDataSet(entries, "Label").apply {
                color = Color.Red.toArgb()
            }

            val lineData = LineData(dataSet)
            lineChart.data = lineData
            lineChart.invalidate() //=> notifyDataSetChanged

            lineChart
        }){

        try {
                Log.d("TAG", "MagLineChart: Update Current thread id: ${Thread.currentThread()}")

                it.data.dataSets[0].addEntry(Entry(sensorData.x, sensorData.y))
                it.lineData.notifyDataChanged()
                it.notifyDataSetChanged()
                it.invalidate() //=> notifyDataSetChanged*/
        } catch(ex: Exception) {
            Log.d("TAG", "MagLineChart: $ex")
        }
    }
}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    LiveGraphTheme {
        Greeting("Android")
    }
}