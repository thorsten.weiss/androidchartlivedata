package de.tw.livegraph

import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import de.tw.magnetometerapp.data.SensorModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.random.Random

@HiltViewModel
class MainViewModel @Inject constructor(@ApplicationContext var appContext: Context) : ViewModel()  {

    //private val _sensorDownFlow = MutableStateFlow<SensorModel>(SensorModel(0F,0F))
    //val sensorDownFlow = _sensorDownFlow.asStateFlow()

    private var rand: Random = Random(1)

    val sensorDownFlow: Flow<SensorModel> = flow<SensorModel> {
        //        Emission from another coroutine is detected.
        //        Child of StandaloneCoroutine{Active}@ebb954d, expected child of StandaloneCoroutine{Completed}@3ea3802.
        //        FlowCollector is not thread-safe and concurrent emissions are prohibited.
        //        To mitigate this restriction please use 'channelFlow' builder instead of 'flow'
        //viewModelScope.launch {
            while (true) {
                delay(1000L)
                Log.d("TAG", "sensorDownFlow: Current thread id: ${Thread.currentThread()}")

                emit(SensorModel(rand.nextFloat(), rand.nextFloat()))
            }
        //}
    }

    init {
        //multi-threading the access
        //viewModelScope.launch (Dispatchers.IO) {
        //}

    }
}