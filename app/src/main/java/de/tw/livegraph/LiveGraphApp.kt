package de.tw.livegraph

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class LiveGraphApp : Application() {
}